.. Sage Cell Server documentation master file, created by
   sphinx-quickstart on Fri May 13 22:12:46 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Sage Cell Server Documentation
=============================================

Contents:

.. toctree::
    :maxdepth: 2

    README
    embedding
    db
    trusted_db
    web_server
    device
    filestore
    interact_protocol
    timing
    js

.. automodule:: misc
    :members:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

